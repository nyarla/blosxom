# Blosxom Plugin:   pagination
# Author:           Naoki Okamura (Nyarla) <thotep@nyarla.net>
# Version:          2008-05-28
# License:          public domain

package pagination;

use strict;
use warnings;

use CGI qw( param );

# -- configurable variables ---------- #

my $page_param = 'page';

# -- package variables --------------- #

our ( $total, $current, $next, $previous, $paginated );

# ------------------------------------ #

sub start {
    return 0 if ( $blosxom::path_info_yr ne q{} );
    return 0 if ( $blosxom::path_info =~ m{\.$blosxom::flavour$} );
    return 1;
}

sub filter {
    my ( $class, $files, $others ) = @_;

    my $category = "$blosxom::datadir/$blosxom::path_info";

    for my $path ( keys %{ $files } ) {
        delete $files->{$path} if ( $path !~ m{^$category} );
    }

    my $all         = scalar( keys %{ $files } );
       $total       = $all / $blosxom::num_entries;
       $total       = int( $total + 1 ) if ( $total !~ m{^\d+$} );
       $current     = ( ( param($page_param) || q{} ) =~ m{(\d+)} )[0];
       $current   ||= 1;

    return 1;
}

sub sort {
    return sub {
        my ( $files, $others ) = @_;
        
        my $form    = $blosxom::num_entries * ( $current - 1 );
        my $to      = scalar( keys %{ $files } ) - 1;
        my @sorted  = sort { $files->{$b} <=> $files->{$a} } keys %{ $files };

        return @sorted[ $form .. $to ];
    }
}

sub head {
    my ( $class, $path, $head_ref ) = @_;

    $previous   = $current - 1 if ( $current > 1 );
    $next       = $current + 1 if ( $current + 1 <= $total );

    $paginated  = 1 if ( $current > 1 );
}

1;
